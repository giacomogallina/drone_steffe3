function plotdrone3(t, l, F1, F2, F3, F4)
%PLOTDRONE3

hold off;

minx = l(1) - 1; maxx = l(1) + 1;
miny = l(3) - 1; maxy = l(3) + 1;
minz = l(5) - 1; maxz = l(5) + 1;

x1 = l(1) - cos(l(7)); x2 = l(1) + cos(l(7));
z1 = l(5) - sin(l(7)); z2 = l(5) + sin(l(7));

plot3([ x1 x2 ], [ l(3) l(3) ], [ z1 z2 ], 'b-'); hold on;

y1 = l(3) - cos(l(9)); y2 = l(3) + cos(l(9));
z1 = l(5) - sin(l(9)); z2 = l(5) + sin(l(9));

plot3([ l(1) l(1) ], [ y1 y2 ], [ z1 z2 ], 'b-');

plot3(l(1)-cos(l(7)), l(3), l(5)-sin(l(7)), 'ro');
plot3(l(1)+cos(l(7)), l(3), l(5)+sin(l(7)), 'ro');
plot3(l(1), l(3)-cos(l(9)), l(5)-sin(l(9)), 'ro');
plot3(l(1), l(3)+cos(l(9)), l(5)+sin(l(9)), 'ro');
lx = maxx - minx; lz = maxz - minz; ly = maxy - miny;
axis([ minx-lx-1 maxx+lx+1 miny-ly-1 maxy+ly+1 minz-lz maxz+lz ]);

% Annotate info
ss = (maxx - minx) / 8;
text(minx-lx-2*ss, maxy+ly-ss, maxz+lz-ss, sprintf('Time: %2.2f seconds', t));
text(minx-lx-2*ss, maxy+ly-ss, maxz+lz-2*ss, sprintf('Vertical speed: %2.1f m/s', l(6)));
text(minx-lx-2*ss, maxy+ly-ss, maxz+lz-3*ss, sprintf('X speed: %2.1f m/s', l(2)));
text(minx-lx-2*ss, maxy+ly-ss, maxz+lz-4*ss, sprintf('Y speed: %2.1f m/s', l(4)));
text(minx-lx-2*ss, maxy+ly-ss, maxz+lz-5*ss, sprintf('θ speed: %2.1f rad/s', l(8)));
text(minx-lx-2*ss, maxy+ly-ss, maxz+lz-6*ss, sprintf('α speed: %2.1f rad/s', l(10)));
text(minx-lx-2*ss, maxy+ly-ss, maxz+lz-7*ss, sprintf('Quota: %2.1f m', l(5)));

if exist('F1', 'var')
    text(minx-lx-2*ss, maxy+ly-ss, maxz+lz-8*ss, sprintf('F1: %2.2e N', F1));
    text(minx-lx-2*ss, maxy+ly-ss, maxz+lz-9*ss, sprintf('F2: %2.2e N', F2));
    text(minx-lx-2*ss, maxy+ly-ss, maxz+lz-10*ss, sprintf('F3: %2.2e N', F3));
    text(minx-lx-2*ss, maxy+ly-ss, maxz+lz-11*ss, sprintf('F4: %2.2e N', F4));
end

fill([ -2 2 2 -2 ], [-2 -2 2 2], 'g');

% 3d building
fill3([ 10 10 10 10 ], [0 0 4 4], [ 0 20 20 0 ], 'y');
fill3([ 10 14 14 10 ], [0 0 0 0], [ 0 0 20 20 ], 'y');
fill3([ 10 14 14 10 ], [0 0 4 4], [ 20 20 20 20 ], 'y'); % top
fill3([ 14 14 14 14 ], [0 0 4 4], [ 0 20 20 0 ], 'y');
fill3([ 10 14 14 10 ], [4 4 4 4], [ 0 0 20 20 ], 'y');

axis square;
drawnow;


end

