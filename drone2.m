function drone2()
% Definizione dei parametri , e della funzione F
% per l ’ equazione differenziale U ’ = F (t , U ) .

	m = 1;
	l = 1;
	I = 2*m*l^2;
	g = 9.8;
	k = 2.5;

	tmax = 100;
	timer = tic ;
	told = 0;
% Configurazione iniziale
	U = [0;0;0;0;0;0];
	while true
		tnew = toc(timer);
		if tnew < told+1/25
			pause(tnew-told-1/25);
		end
% Impostiamo la forza verticale
		F1 = 10;
		F2 = 10;
% Integriamo fino a tnew , buttiamo via
% gli istanti intermedi.
		[~, UU] = ode45(@(t,U) F(t,U,F1,F2,m,l,I,g,k), [told,tnew], U) ;
		U = UU(end,:);
		U = U(:);
		plotdrone1(tnew, U, F1, F2);
		told = tnew;
		if tnew >= tmax
			break;
		end
	end

end


function Un = F(t,U,F1,F2,m,l,I,g,k)

	Un = ...
		[ U(2) ...
		, -(F1+F2)*sin(U(5))/(2*m) - k*U(2)/(2*m) ...
		, U(4) ...
		, (F1+F2)*cos(U(5))/(2*m) - g - k*U(4)/(2*m) ...
		, U(6) ...
		, (F2-F1)*l/I - k*l*U(6)/I ...
		];

end
