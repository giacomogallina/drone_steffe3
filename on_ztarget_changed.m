function on_ztarget_changed(control, data)
%ON_ZTARGET_CHANGED 

	global ztarget;
	
	value = sscanf(get(control,'string'), '%f');
	
	if ~isempty(value)
	    ztarget = value;
	end

end

