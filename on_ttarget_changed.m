function on_ttarget_changed(control, data)
%ON_TTARGET_CHANGED 

	global ttarget;

	value = sscanf(get(control, 'string'), '%f');

	if ~isempty(value)
	    ttarget = value;
	end

end

