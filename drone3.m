function drone3()
% Definizione dei parametri , e della funzione F
% per l ’ equazione differenziale U ’ = F (t , U ) .

	m = 1;
	l = 1;
	I = 2*m*l^2;
	g = 9.8;
	z0 = 10;
	h0 = (2*pi/360)*30;
	k = 2.5;

	tmax = 100;
	timer = tic ;
	told = 0;
% Configurazione iniziale
	U = [0;0;0;0;0;0];
	while true
		tnew = toc(timer);
		if tnew < told+1/25
			pause(tnew-told-1/25);
		end
% Impostiamo la forza verticale
		Ft = (2*m*g - (U(3)-z0) -(U(5)-h0) - k*U(4))/cos(U(5));
		F1 = (Ft + (U(5)-h0) + k*U(6))/2;
		F2 = (Ft - (U(5)-h0) - k*U(6))/2;
% Integriamo fino a tnew , buttiamo via
% gli istanti intermedi.
		[~, UU] = ode45(@(t,U) F(t,U,F1,F2,m,l,I,g), [told,tnew], U) ;
		U = UU(end,:);
		U = U(:);
		plotdrone1(tnew, U, F1, F2);
		told = tnew;
		if tnew >= tmax
			break;
		end
	end

end


function Un = F(t,U,F1,F2,m,l,I,g)

	Un = ...
		[ U(2) ...
		, -(F1+F2)*sin(U(5))/(2*m) ...
		, U(4) ...
		, (F1+F2)*cos(U(5))/(2*m)-g ...
		, U(6) ...
		, (F2-F1)*l/I ...
		];

end
