function plotdrone1(t, l, F1, F2)
%PLOTDRONE1 Mostra la posizione del drone sullo schermo. 
%
% PLOTDRONE1(T, L) mostra il drone al tempo T, date le coordinate
% generalizzate nel vettore L, che deve avere 6 elementi; il vettore L deve
% contenere le coordinate X, X', Z, Z', theta, theta', dove ' indica la 
% derivata prima. 
%
% Opzionalmente, si può passare alla funzione anche le forze F1 ed F2
% esercitate dalle eliche, che verranno mostrate sullo schermo. 

% Determino la posizione dove si trova il drone
minx = l(1) - 1; maxx = l(1) + 1;
minz = l(3) - 1; maxz = l(3) + 1;

hold off;

tt = linspace(0, 1);

x1 = l(1) - cos(l(5)); x2 = l(1) + cos(l(5));
y1 = l(3) - sin(l(5)); y2 = l(3) + sin(l(5));

fill([ x1 + tt .* (x2 - x1), x1 + tt(end:-1:1) .* (x2 - x1) ], ...
     [ y1 + tt .* (y2 - y1) - tt .* (1 - tt), y1 + tt(end:-1:1) .* (y2 - y1) + tt .* (1 - tt) ], 'b');
hold on;

plot(l(1)-cos(l(5)), l(3)-sin(l(5)), 'ro');
plot(l(1)+cos(l(5)), l(3)+sin(l(5)), 'ro');
lx = maxx - minx; lz = maxz - minz;
axis([ minx-lx-1 maxx+lx+1 minz-lz maxz+lz ]);

% Annotate info
ss = (maxx - minx) / 8;
text(minx-lx-2*ss, maxz+lz-ss, sprintf('Time: %2.2f seconds', t));
text(minx-lx-2*ss, maxz+lz-2*ss, sprintf('Vertical speed: %2.1f m/s', l(4)));
text(minx-lx-2*ss, maxz+lz-3*ss, sprintf('Hor. speed: %2.1f m/s', l(2)));
text(minx-lx-2*ss, maxz+lz-4*ss, sprintf('Rot. speed: %2.1f rad/s', l(6)));
text(minx-lx-2*ss, maxz+lz-5*ss, sprintf('Quota: %2.1f m', l(3)));

if exist('F1', 'var')
    text(minx-lx-2*ss, maxz+lz-6*ss, sprintf('F1: %2.2e N', F1));
    text(minx-lx-2*ss, maxz+lz-7*ss, sprintf('F2: %2.2e N', F2));
    
    %quiver(l(1) - cos(l(5)), l(3) - sin(l(5)), ...
    %       -F1 * sin(l(5)), F1 * cos(l(5)));
end

plot([ minx-lx-1 maxx+lx+1 ], [ -1/4 -1/4 ], 'g-');


axis square;
drawnow;


end

